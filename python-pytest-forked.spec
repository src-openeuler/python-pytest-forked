%global _empty_manifest_terminate_build 0
Name:		python-pytest-forked
Version:	1.6.0
Release:	3
Summary:	run tests in isolated forked subprocesses
License:	MIT
URL:		https://github.com/pytest-dev/pytest-forked
Source0:	https://files.pythonhosted.org/packages/8c/c9/93ad2ba2413057ee694884b88cf7467a46c50c438977720aeac26e73fdb7/pytest-forked-1.6.0.tar.gz
# https://github.com/pytest-dev/pytest-forked/commit/b2742322d39ebda97d5170922520f3bb9c73f614
Patch0: 	Sync-expected-test_xfail-s-xpassed-summary-to-Pytest.patch
BuildArch:	noarch

%description
pytest-forked: run each test in a forked subprocess .. warning:: this is a
extraction of the xdist --forked module, future maintenance beyond the bare
minimum is not planned until a new maintainer is found. This plugin **does not
work on Windows*because there's no fork support. --forked: run each test in a
forked subprocess to survive SEGFAULTS or otherwise dying processes.|python|
|version| |ci| |pre-commit| |black|.. |versio .. |ci| image:: .

%package -n python3-pytest-forked
Summary:	run tests in isolated forked subprocesses
Provides:	python-pytest-forked = %{version}-%{release}
# Base build requires
BuildRequires:	python3-devel
BuildRequires:	python3-setuptools
BuildRequires:	python3-pbr
BuildRequires:	python3-pip
BuildRequires:	python3-wheel
# General requires
BuildRequires:	python3-py
BuildRequires:	python3-pytest
BuildRequires:	python3-setuptools_scm
# General requires
Requires:	python3-py
Requires:	python3-pytest
%description -n python3-pytest-forked
pytest-forked: run each test in a forked subprocess .. warning:: this is a
extraction of the xdist --forked module, future maintenance beyond the bare
minimum is not planned until a new maintainer is found. This plugin **does not
work on Windows*because there's no fork support. --forked: run each test in a
forked subprocess to survive SEGFAULTS or otherwise dying processes.|python|
|version| |ci| |pre-commit| |black|.. |versio .. |ci| image:: .

%package help
Summary:	Development documents and examples for pytest-forked
Provides:	python3-pytest-forked-doc
%description help
pytest-forked: run each test in a forked subprocess .. warning:: this is a
extraction of the xdist --forked module, future maintenance beyond the bare
minimum is not planned until a new maintainer is found. This plugin **does not
work on Windows*because there's no fork support. --forked: run each test in a
forked subprocess to survive SEGFAULTS or otherwise dying processes.|python|
|version| |ci| |pre-commit| |black|.. |versio .. |ci| image:: .

%prep
%autosetup -n pytest-forked-%{version} -p1

%build
%py3_build

%install
%py3_install
install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.rst ]; then cp -af README.rst %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.md ]; then cp -af README.md %{buildroot}/%{_pkgdocdir}; fi
if [ -f README.txt ]; then cp -af README.txt %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
	find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
	find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
	find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
	find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
	find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%check
PYTHONPATH=%{buildroot}%{python3_sitelib} py.test-%{python3_version} testing
rm -f %{buildroot}%{python3_sitelib}/pytest_forked/__pycache__/__init__.cpython-*-pytest-*.pyc

%files -n python3-pytest-forked -f filelist.lst
%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Tue Jan 21 2025 yaoxin <1024769339@qq.com> - 1.6.0-3
- Sync expected test_xfail's xpassed summary to Pytest 8

* Sun Jul 23 2023 wangkai <13474090681@163.com> - 1.6.0-2
- Fix build error for python3.11

* Fri Mar 24 2023 wangjunqi <wangjunqi@kylinos.cn> - 1.6.0-1
- Update package to version 1.6.0

* Thu Dec 29 2022 wulei <wulei80@h-partners.com> - 1.4.0-3
- Rectify the compilation failure

* Wed Jun 22 2022 renliang <renliang@uniontech.com> - 1.4.0-2
- To modify check, run check

* Thu May 19 2022 renliang <renliang@uniontech.com> - 1.4.0-1
- Upgrade package python3-pytest-forked to version 1.4.0

* Wed Aug 25 2021 liyanan <liyanan32@huawei.com> - 1.0.2-2
- fix test case fail with Pytest6 and Pytest5

* Tue Jul 06 2021 wangdi <wangdi@kylinos.cn> - 1.0.2-1
- Init package for version 1.0.2
